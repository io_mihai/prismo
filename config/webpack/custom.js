module.exports = {
  resolve: {
    alias: {
      // It's here becase vue-snotify was not working fine with the runtime-only
      // builds. More info here: https://github.com/artemsky/vue-snotify/issues/14#issuecomment-347799054
      // @todo: Monitor the issue and resolve this when ready
      vue$: 'vue/dist/vue.esm.js'
    }
  }
}

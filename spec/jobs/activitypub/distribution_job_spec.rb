# frozen_string_literal: true

require 'rails_helper'

describe ActivityPub::DistributionJob do
  subject { described_class.new }

  let(:remote_account) do
    create(:account, :remote, inbox_url: 'https://example.com')
  end

  let!(:story) { create(:story) }

  let!(:remote_comment) do
    create(:comment, :remote, account: remote_account, story: story)
  end

  let!(:local_comment) { create(:comment, story: story) }

  before do
    stub_jsonld_contexts!
  end

  describe '#perform' do
    before do
      allow(ActivityPub::DeliveryJob).to receive(:perform_later)
    end

    it 'delivers to followers' do
      subject.perform('Comment', local_comment.id)

      expect(ActivityPub::DeliveryJob)
        .to have_received(:perform_later)
        .with(any_args, local_comment.account_id, 'https://example.com')
    end
  end
end

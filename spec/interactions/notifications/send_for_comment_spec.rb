# frozen_string_literal: true

require 'rails_helper'

describe Notifications::SendForComment do
  describe '#run' do
    subject { described_class.run comment: comment }

    context 'when comment is a reply to another user comment' do
      let(:story) { create(:story) }
      let!(:parent_comment) { create(:comment, story: story) }
      let!(:child_comment) do
        create(:comment, parent: parent_comment, story: story)
      end
      let(:comment) { child_comment }

      it 'sends notification to parent comment author' do
        expect(CreateNotificationJob)
          .to receive(:call)
          .with('comment_reply',
                author: child_comment.account,
                recipient: parent_comment.account,
                notifable: comment,
                context: parent_comment.story)

        subject
      end
    end

    context 'when comment is a reply to the same user comment' do
      let(:story) { create(:story) }
      let!(:parent_comment) { create(:comment, story: story) }
      let!(:child_comment) do
        create(:comment, parent: parent_comment,
                         story: story,
                         account: parent_comment.account)
      end
      let(:comment) { child_comment }

      it 'does not send notification to parent comment author' do
        expect(CreateNotificationJob)
          .to_not receive(:call)

        subject
      end
    end

    context 'when comment is a reply to another user story' do
      let(:story) { create(:story) }
      let!(:child_comment) { create(:comment, story: story) }
      let(:comment) { child_comment }

      it 'sends notification to story author' do
        expect(CreateNotificationJob)
          .to receive(:call)
          .with('story_reply',
                author: child_comment.account,
                recipient: story.account,
                notifable: comment,
                context: story)

        subject
      end
    end

    context 'when comment is a reply to the same user comment' do
      let(:story) { create(:story) }
      let!(:child_comment) do
        create(:comment, story: story, account: story.account)
      end
      let(:comment) { child_comment }

      it 'does not send notification to parent comment author' do
        expect(CreateNotificationJob)
          .to_not receive(:call)

        subject
      end
    end
  end
end

# frozen_string_literal: true

require 'rails_helper'

describe Stories::Delete do
  describe '#run' do
    before do
      stub_jsonld_contexts!

      allow(ActivityPub::DeliveryJob).to receive(:perform_later)
    end

    subject do
      described_class.run(story: story)
    end

    let!(:remote_alice) { create(:account, inbox_url: 'https://alice.com/inbox') }
    let!(:remote_bob) { create(:account, inbox_url: 'https://bob.com/inbox') }

    context 'when story is local' do
      let!(:account) { create(:account) }
      let!(:story) { create(:story, account: account) }

      it 'sends a delete activity to all known inboxes' do
        subject

        expect(ActivityPub::DeliveryJob)
          .to have_received(:perform_later)
          .with(any_args, account.id, 'https://alice.com/inbox')

        expect(ActivityPub::DeliveryJob)
          .to have_received(:perform_later)
          .with(any_args, account.id, 'https://bob.com/inbox')
      end
    end

    context 'when story is remote' do
      let!(:story) { create(:story, :not_local) }

      it 'does not send delete activity to known inboxes' do
        subject

        expect(ActivityPub::DeliveryJob).to_not have_received(:perform_later)
      end
    end
  end
end

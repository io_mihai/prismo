# frozen_string_literal: true

FactoryBot.define do
  factory :like do
    account
    association :likeable, factory: :story
  end
end

require 'rails_helper'

describe StoriesQuery do
  let(:account) { create(:account) }
  let(:actor) { create(:user, account: account) }
  let(:story) { create(:story) }

  describe '#for_dashboard' do
    let(:call) { described_class.new.for_dashboard(actor.account) }

    context 'when actor is not following anything' do
      it 'does not return any story' do
        expect(call).to be_empty
      end
    end

    context 'when actor follows story author' do
      before do
        create(:follow, follower: account, following: story.account)
      end

      it 'returns story from followed author' do
        expect(call).to include(story)
      end
    end

    context 'when actor follows any tag assigned to a story' do
      before do
        create(:follow, follower: account, following: story.tags.first)
      end

      it 'returns story from followed tag' do
        expect(call).to include(story)
      end
    end

    context 'when actor follows all tags assigned to a story' do
      before do
        story.tags.each do |tag|
          create(:follow, follower: account, following: tag)
        end
      end

      it 'returns story from followed tag - just once' do
        expect(call).to eq [story]
      end
    end

    context 'when actor follows any tag assigned to a story and the story author' do
      before do
        create(:follow, follower: account, following: story.tags.first)
        create(:follow, follower: account, following: story.account)
      end

      it 'returns story from followed tag and account - once' do
        expect(call).to eq [story]
      end
    end
  end
end

# frozen_string_literal: true

require 'rails_helper'

feature 'Signing in' do
  let(:sign_in_page) { SignInPage.new }
  let(:home_page) { HomePage.new }

  let(:user) do
    create(:user, :with_account, password: 'TestPass', confirmed_at: Time.zone.now)
  end

  let(:unconfirmed_user) do
    create(:user, :with_account, :unconfirmed, password: 'TestPass')
  end

  before do
    sign_in_page.load
  end

  scenario 'fully valid user signs in' do
    sign_in_page.sign_in_using(user.email, 'TestPass')
    expect(home_page).to be_displayed
  end

  scenario 'sign in with wrong credentials' do
    sign_in_page.sign_in_using(user.email, 'WrongPass')

    expect(sign_in_page).to be_displayed
    expect(sign_in_page).to have_content 'Invalid Email or password'
  end

  scenario 'sign in as valid but unconfirmed user' do
    sign_in_page.sign_in_using(unconfirmed_user.email, 'TestPass')

    expect(sign_in_page).to be_displayed
    expect(sign_in_page)
      .to have_content 'You have to confirm your email address before continuing'
  end
end

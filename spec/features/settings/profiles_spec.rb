require 'rails_helper'

feature 'User profile settings', js: true do
  let(:settings_profile_page) { Settings::ProfilePage.new }
  let(:sign_in_page) { SignInPage.new}
  let(:home_page) { HomePage.new }

  let!(:account) { create(:account) }
  let!(:user) { create(:user, account: account) }

  def sign_user_in
    sign_in_page.load
    sign_in_page.sign_in_using(user.email, 'TestPass')
  end

  before do
    sign_user_in
    settings_profile_page.load
  end

  scenario 'user changes details of his account' do
    settings_profile_page.display_name_field.set 'Changed display name'
    settings_profile_page.bio_field.set 'Changed bio'
    settings_profile_page.submit_button.click

    expect(settings_profile_page).to have_content 'Changes have been saved'
  end

  scenario 'user deletes their account' do
    expect(Accounts::SuspendJob).to receive(:perform_later).with(account.id, true)
    settings_profile_page.delete_account 'TestPass'
    expect(home_page).to be_displayed
  end

  scenario 'user enters incorrect password when deleting account' do
    expect(Accounts::SuspendJob).to_not receive(:perform_later)
    settings_profile_page.delete_account 'WrongPassword'
    expect(settings_profile_page).to be_displayed
  end
end

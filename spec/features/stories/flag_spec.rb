# frozen_string_literal: true

require 'rails_helper'

feature 'Flagging on story' do
  let(:sign_in_page) { SignInPage.new }
  let(:home_page) { HomePage.new }
  let(:story_page) { Stories::ShowPage.new }
  let(:story_new_flag_page) { Stories::NewFlagPage.new }
  let(:story_edit_flag_page) { Stories::EditFlagPage.new }
  let(:admin_flags_page) { Admin::Flags::IndexPage.new }

  let(:user) { create(:user, :with_account, password: 'TestPass', confirmed_at: Time.current) }
  let(:admin) { create(:user, :admin, :with_account, password: 'TestPass', confirmed_at: Time.current) }
  let!(:story) { create(:story) }

  def sign_user_in
    sign_in_page.load
    sign_in_page.sign_in_using(user.email, 'TestPass')
  end

  def sign_admin_in
    sign_in_page.load
    sign_in_page.sign_in_using(admin.email, 'TestPass')
  end

  scenario 'user flags a story not flagged by him yet', js: true do
    home_page.load

    sign_user_in
    expect(home_page).to have_stories

    # Go to flag form
    story_el = home_page.stories.first
    story_el.open_more_dropdown
    story_el.flag_link.click

    # Create flag
    expect(story_new_flag_page).to be_loaded
    story_new_flag_page.wait_until_new_flag_form_visible
    story_new_flag_page.new_flag_form.flag_summary_input.set 'Flag summary'
    story_new_flag_page.new_flag_form.submit

    expect(story_page).to be_loaded
    expect(story_page).to have_content 'Flag has been created'

    # Sign in as admin
    story_page.navbar.open_user_dropdown
    story_page.navbar.sign_out
    sign_admin_in

    # Check if flag has been created
    admin_flags_page.load
    expect(admin_flags_page).to have_flags(count: 1)
  end

  scenario 'user flags a story already flagged by him', js: true do
    home_page.load

    sign_user_in
    expect(home_page).to have_stories

    # Go to flag form
    story_el = home_page.stories.first
    story_el.open_more_dropdown
    story_el.flag_link.click

    # Create flag
    expect(story_new_flag_page).to be_loaded
    story_new_flag_page.new_flag_form.flag_summary_input.set 'Flag summary'
    story_new_flag_page.new_flag_form.submit

    expect(story_page).to be_loaded
    expect(story_page).to have_content 'Flag has been created'

    # Go to flagging again
    story_el = home_page.stories.first
    story_el.open_more_dropdown
    story_el.flag_link.click

    # Check if there is info in place and if form is filled
    expect(story_edit_flag_page).to be_loaded
    expect(story_edit_flag_page.edit_flag_form.flag_summary_input.value).to eq 'Flag summary'
  end
end

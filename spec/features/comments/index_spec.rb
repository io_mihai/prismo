# frozen_string_literal: true

require 'rails_helper'

feature 'Browsing comments list' do
  let(:sign_in_page) { SignInPage.new }
  let(:home_page) { HomePage.new }
  let(:comments_page) { Comments::IndexPage.new }

  let!(:user) { create(:user, :with_account) }
  let!(:comment) { create(:comment) }

  def sign_user_in
    sign_in_page.load
    sign_in_page.sign_in_using(user.email, 'TestPass')
  end

  def it_behaves_like_valid_comments_page
    expect(comments_page).to be_displayed
    comments_page.wait_for_comments
    expect(comments_page).to have_comments
  end

  scenario 'guest user browses comments page' do
    home_page.load

    expect(home_page).to be_displayed
    home_page.navbar.comments_link.click
    it_behaves_like_valid_comments_page
  end

  scenario 'signed in user browses comments page' do
    sign_user_in

    expect(home_page).to be_displayed
    home_page.navbar.comments_link.click
    it_behaves_like_valid_comments_page
  end
end

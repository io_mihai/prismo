# frozen_string_literal: true

require 'rails_helper'

feature 'Followed users dashboard' do
  let(:account_page) { Accounts::ShowPage.new }
  let(:home_page) { HomePage.new }
  let(:sign_in_page) { SignInPage.new }
  let(:dashboards_hot_page) { DashboardsHotPage.new }

  let(:user) { create(:user, :with_account) }
  let(:actor) { create(:user, :with_account) }
  let!(:story) { create(:story, account: user.account) }

  def sign_user_in
    sign_in_page.load
    sign_in_page.sign_in_using(actor.email, 'TestPass')
  end

  scenario 'signed-in user wants to see posts from followed accounts', js: true do
    sign_user_in

    dashboards_hot_page.load
    expect(dashboards_hot_page).to_not have_stories

    account_page.load(username: user.account.username)

    expect(account_page).to be_displayed
    expect(account_page).to have_follow_button

    account_page.click_follow_button
    expect(account_page.follow_button).to have_content 'FOLLOWING'

    dashboards_hot_page.load
    expect(dashboards_hot_page).to have_stories(count: 1)
  end
end

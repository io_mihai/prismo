# frozen_string_literal: true

require 'rails_helper'

feature 'Searching stories' do
  let(:search_stories_page) { Search::StoriesPage.new }

  let!(:story_a) { create(:story, title: 'Title of story A', description: 'Description of story A') }
  let!(:story_b) { create(:story, title: 'Title of story B', description: 'Description of story B') }

  scenario 'user searches stories' do
    search_stories_page.load
    expect(search_stories_page).to have_content 'Please write your search query in the input above'

    # Enters wrong query
    search_stories_page.search('wrongquery')
    expect(search_stories_page).to have_content 'No results have been found. Maybe try changing search criteria?'

    # Enters title query
    search_stories_page.search('Title of story A')
    expect(search_stories_page).to have_stories(count: 1)
    expect(search_stories_page.stories.first).to have_content story_a.title

    # Enters description query
    search_stories_page.search('Description of story B')
    expect(search_stories_page).to have_stories(count: 1)
    expect(search_stories_page.stories.first).to have_content story_b.description
  end
end

class Settings::ProfilePage < SitePrism::Page
  set_url '/settings/profile'
  set_url_matcher %r{\/settings\/profile}

  element :display_name_field, '#account_display_name'
  element :bio_field, '#account_bio'
  element :submit_button, 'input[value="Save account"]'

  element :current_password_field, 'input#accounts_delete_current_password'
  element :delete_account_button, '[data-action="profile#showDeleteModal"]'
  element :confirm_delete_button, '#new_accounts_delete [type="submit"]'

  def delete_account(password)
    delete_account_button.click
    current_password_field.set password
    confirm_delete_button.click
  end
end

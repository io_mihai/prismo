# frozen_string_literal: true

module Stories
  class NewPage < SitePrism::Page
    set_url '/posts/new'
    set_url_matcher %r{\/posts(\/new)?\z}

    element :url_field, 'input#story_url'
    element :title_field, 'input#story_title'
    element :tag_list_field, '.tagify__input'
    element :description_field, 'textarea#story_description'
    element :submit_button, '[type="submit"]'

    elements :tags, '.tagify tag'

    def clear_tags
      tags.count.times { tag_list_field.native.send_keys(:backspace) }
    end

    def tags=(tags = [])
      clear_tags
      add_tags(tags)
    end

    def add_tags(tags = [])
      tags.each { |t| add_tag(t) }
    end

    def add_tag(tag)
      tag_list_field.set(tag)
      tag_list_field.native.send_keys(:return)
    end
  end
end

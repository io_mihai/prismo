module Search
  class PostsController < BaseController
    before_action :set_account_liked_story_ids

    def index
      posts = StoriesQuery.new.all

      posts = posts.search(params[:q])

      @posts = posts.page(params[:page])
    end
  end
end

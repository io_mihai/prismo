module Search
  class CommentsController < BaseController
    before_action :set_account_liked_comment_ids

    def index
      comments = CommentsQuery.new.all

      comments = comments.search(params[:q])

      @comments = comments.page(params[:page])
    end
  end
end

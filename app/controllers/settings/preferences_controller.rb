# frozen_string_literal: true

module Settings
  class PreferencesController < ApplicationController
    before_action :authenticate_user!

    layout 'settings'

    def show
      @preferences = Settings::UpdatePreferencesForm.new(user: current_user)
    end

    def update
      @preferences = Settings::UpdatePreferencesForm.run(
        settings_params.merge(user: current_user)
      )

      if @preferences.valid?
        redirect_to settings_preferences_path, notice: 'Preferences updated'
      else
        render :show
      end
    end

    private

    def settings_params
      params.require(:settings).permit(:theme)
    end
  end
end

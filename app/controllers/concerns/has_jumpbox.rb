# frozen_string_literal: true

module HasJumpbox
  extend ActiveSupport::Concern

  included do
    helper_method :current_jumpbox_link
  end

  private

  def set_jumpbox_link(link)
    @current_jumpbox_link = Jumpbox.instance.resolve(link)
  end

  def current_jumpbox_link
    @current_jumpbox_link || Jumpbox::DEFAULT_LINK
  end
end

# frozen_string_literal: true

module WellKnown
  class XNodeinfo2Controller < ActionController::Base
    def show
      render json: XNodeinfo2Serializer.new
    end
  end
end

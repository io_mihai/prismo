# frozen_string_literal: true

class ActivityPub::ActorSerializer < ActivityPub::BaseSerializer
  def data
    result = {
      id: ActivityPub::TagManager.instance.uri_for(object),
      type: 'Person',
      name: object.display_name,
      preferredUsername: object.username,
      summary: object.bio,
      inbox: activitypub_account_inbox_url(object),
      outbox: outbox_activitypub_account_url(object),
      url: account_url(object),
      publicKey: public_key
    }

    result[:icon] = icon

    result
  end

  private

  def public_key
    {
      id: ActivityPub::TagManager.instance.uri_for(object) + '#main-key',
      owner: ActivityPub::TagManager.instance.uri_for(object),
      publicKeyPem: object.public_key
    }
  end

  def icon
    return nil if !object.avatar_data? || !object.avatar_attacher.stored?

    ActivityPub::ImageSerializer.new(object.avatar).data
  end
end

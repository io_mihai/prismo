# frozen_string_literal: true

module Stories
  class SendWebmentionJob < ApplicationJob
    queue_as :webmentions

    def perform(story_id)
      story = Story.find(story_id).decorate

      logger.info "Discovering webmention for #{story.url}"
      endpoint = Webmention::Endpoint.discover(story.url)

      if endpoint.present?
        logger.info "Endpoint found. Sending webmention from #{story.local_url} to #{story.url}"
        Webmention::Client.send_mention(endpoint, story.local_url, story.url)
      else
        logger.info 'No endpoint found.'
      end
    end
  end
end

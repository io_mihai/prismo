# frozen_string_literal: true

class StorySearch
  include SearchObject.module(:kaminari, :enum)

  scope { Story.all.includes(:tags, :url_meta, :account) }

  per_page 25
  max_per_page 25

  option :order, enum: %w(score created_at)

  option(:tags) { |scope, value| scope.tagged_with(names: value) }
  option(:account_id) { |scope, value| scope.where(account_id: value) }
  option(:domain) { |scope, value| scope.where(domain: value) }

  def apply_order_with_score(scope)
    scope.order('ranking(likes_count, created_at::timestamp, 3) DESC')
  end

  def apply_order_with_created_at(scope)
    scope.order(created_at: :desc)
  end
end

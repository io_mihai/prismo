# frozen_string_literal: true

class ActivityPub::Activity::Delete < ActivityPub::Activity
  def perform
    comment = Comment.find_by(uri: object_uri, account: account)
    return unless comment.present?

    Comments::Delete.run!(comment: comment)
  end
end

import { Application } from "stimulus"
import { definitionsFromContext } from "stimulus/webpack-helpers"
import Turbolinks from "turbolinks"
import axios from 'axios'
import Rails from 'rails-ujs'
import errorStatusResolver from './lib/error_status_resolver'

const sentryDsnEl = document.querySelector("meta[name=sentry-dsn]")
if (sentryDsnEl && typeof(Raven) !== 'undefined') {
  Raven.config(sentryDsnEl.content).install()
}

// Load spectre.css
import 'feather-icon-font/dist/feather'
import '../css/application.scss'

// Init rails UJS
Rails.start()

// Init turbolinks
Turbolinks.start()

// Init axios
const csrfEl = document.querySelector("meta[name=csrf-token]")
const csrfToken = csrfEl ? csrfEl.content : null
axios.defaults.headers.common['X-CSRF-Token'] = csrfToken

axios.interceptors.response.use((response) => {
  return response;
}, (error) => {
  errorStatusResolver(error.response.status)
  return Promise.reject(error)
})

// Init app
// This needs to be wrapped in DOMContentLoaded event as secrets are passed
// through the meta tags. This should be removed (and left in it's original form)
// when we deal with that properly.
document.addEventListener("DOMContentLoaded", function () {
  const application = Application.start()
  const context = require.context("./controllers", true, /\.js$/)
  application.load(definitionsFromContext(context))
})

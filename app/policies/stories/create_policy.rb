# frozen_string_literal: true

class Stories::CreatePolicy < StoryPolicy
  def update_url?
    true
  end
end

# frozen_string_literal: true

class AccountPolicy < ApplicationPolicy
  def index?
    user.present? && user.is_admin?
  end

  def toggle_follow?
    user.present?
  end

  def suspend?
    user.present? && user != record.user && !record.suspended?
  end

  def silence?
    user.present? && user != record.user && !record.suspended?
  end

  def unsilence?
    user.present? && user != record.user && !record.suspended?
  end
end

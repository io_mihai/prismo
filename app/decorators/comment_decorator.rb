# frozen_string_literal: true

class CommentDecorator < Draper::Decorator
  delegate_all

  def to_flag_title
    'Comment'
  end

  def excerpt
    h.strip_tags(object.body).truncate(200)
  end

  def path
    h.comment_path(object)
  end

  def to_meta_tags
    {
      title: object.story.title,
      description: excerpt,
      alternate: [{
        href: h.comment_url(object), type: 'application/activity+json'
      }],
      og: {
        image: (object.story.thumb_url(:size_200) if object.story.thumb.present?)
      }
    }
  end
end

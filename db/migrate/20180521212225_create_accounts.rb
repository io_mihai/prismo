class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.string :username, default: "", null: false
      t.string :domain
      t.string :secret, default: "", null: false
      t.text :private_key
      t.text :public_key, default: "", null: false
      t.string :remote_url, default: "", null: false
      t.string :salmon_url, default: "", null: false
      t.string :hub_url, default: "", null: false
      t.string :display_name, default: "", null: false
      t.string :uri, default: "", null: false
      t.string :url
      t.datetime :subscription_expires_at
      t.datetime :last_webfingered_at
      t.string :inbox_url, default: "", null: false
      t.string :outbox_url, default: "", null: false
      t.string :shared_inbox_url, default: "", null: false
      t.integer :protocol, default: 0, null: false
      t.boolean :memorial, default: false, null: false
      t.string :actor_type

      t.index ["uri"], name: "index_accounts_on_uri"
      t.index ["url"], name: "index_accounts_on_url"
      t.index ["username", "domain"], name: "index_accounts_on_username_and_domain", unique: true

      t.timestamps
    end
  end
end

class MakeFollowsPolymorphic < ActiveRecord::Migration[5.2]
  def change
    rename_column :follows, :account_id, :follower_id
    add_column :follows, :follower_type, :string
    rename_column :follows, :target_account_id, :following_id
    add_column :follows, :following_type, :string

    Follow.update_all(follower_type: 'Account')
    Follow.update_all(following_type: 'Account')
  end
end
